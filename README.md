# Avenue Code Integration Challenge

This challenge consists of creating Rest services.
created in the same project using Anypoint Studio, the IDE for Mule ESB (www.mulesoft.org). You can download it from https://www.mulesoft.com/mule-esb-enterprise-30-day-trial. Make sure you use the 3.7+ version of Mule ESB.

We believe that without any previous experience with Mule you will still be able to use it because of its intuitive IDE. If you have any difficulties, check their documentation (https://developer.mulesoft.com/docs/display/current/Home) and further resources as YouTube videos.

This source contains the basic template for your implementation.
Mule 3.7+ and Anypoint Studio are compatible only with JDK 1.7+.

## Rest Api service

### 1 - Create a RAML containing the following operations:

* createUser
	* used to  insert the new employee details to the user table
* removeUser
	*  removes user deatils from the user table,address, and communication tbale

*updateUser:
   * this method is used to update the user's address and communication details
*users:
 * this method is to get the  users info from tables
 
 
we have implemented this project using Mysql database

 before start this project make sure you must have mysql database and make sure you run the  script from query's txt file  which is in root folder
 

