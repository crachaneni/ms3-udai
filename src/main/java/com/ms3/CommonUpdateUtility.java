package com.ms3;

import java.util.Map;

public class CommonUpdateUtility {
	public String update(Map<String, String> map) {
		String query = "";
		int count = 0;

		{
			for (Map.Entry<String, String> entry : map.entrySet()) {
				count++;
				query += " " + entry.getKey() + "='" + entry.getValue() + "'";
				if (count < map.size()) {
					query += ",";

				}

			}
			query = (query != "") ? "set " + query + ""  : "";
			return query;

		}
	}
}
