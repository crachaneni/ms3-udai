CREATE TABLE IF NOT EXISTS user (
    userId int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    fname varchar(255),
    lname varchar(255),
    dob varchar(20),
    gender varchar(1),
    title varchar(100)
   
);


CREATE TABLE IF NOT EXISTS address (
    addressID int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    type varchar(255),
    number int,
    street varchar(255),
    unit varchar(255),
    city varchar(50),
    state varchar(50),
    zipcode varchar(10)
    userId INT FOREIGN KEY REFERENCES user(userId)
;

CREATE TABLE IF NOT EXISTS communication (
    communicationId int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    type varchar(255),
    value varchar(100),
    preferred varchar(10),
    userId INT FOREIGN KEY REFERENCES user(userId)
);
